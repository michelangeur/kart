// VARIABLES
const cart = document.querySelector('#carrito')
const courses = document.querySelector('#lista-cursos')
const listCourses = document.querySelector('#lista-carrito tbody')
const emptyCartBtn = document.querySelector('#vaciar-carrito')
const input = document.querySelector('#buscador')
const inputSubmit = document.querySelector('.formulario')




const removeCourse = ( e ) => {
    //'use strict'
    e.preventDefault();

    let course,
        courseId;
    if(e.target.classList.contains('borrar-curso')){
        e.target.parentElement.parentElement.remove()
        course = e.target.parentElement.parentElement;
        courseId = course.querySelector('a').getAttribute('data-id')
    } 

    removeCourseLocalStorage( courseId )
}

// EMPTY SHOPPING CART
const emptyCart = () => {
    //listCourses.innerHTML = '';
    while(listCourses.firstChild){
        listCourses.removeChild(listCourses.firstChild)
    }

    emptyLocalStorage();

    return false;
}

function emptyLocalStorage() {
    localStorage.clear()
}

/** @todo implement fetch api
 * filter all courses
 * render courses according to input
 */
function handleSubmit( e ){
    e.preventDefault()
    console.log( input.value )
    input.value = ""
}

// EVENT LISTENERS
loadEventListeners();

function loadEventListeners(){

    inputSubmit.addEventListener('submit', handleSubmit )
    
    // called when clicked on cart
    courses.addEventListener('click', buyCourse)//add course to cart and DOM

    cart.addEventListener('click', removeCourse) // remove course
    
    emptyCartBtn.addEventListener('click', emptyCart) // empty cart

    document.addEventListener('DOMContentLoaded', readLS )
}

// FUNCTIONS
function buyCourse( e ){
    
    e.preventDefault()
    //  check if any course exists
    if( e.target.classList.contains('agregar-carrito') ){
        const course = e.target.parentElement.parentElement;

        readCourseData( course ) // fn handles picking course
        
    }
}

// create object course to display
function readCourseData( course ){
    const infoCourse = {
        image: course.querySelector('img').src,
        title: course.querySelector('h4').textContent,
        price: course.querySelector('.precio span').textContent,
        id: course.querySelector('a').getAttribute('data-id')
    }

    insertCart(infoCourse)

}

// create tr to be inserted into tbody
const insertCart = ( course ) => {

    const row = document.createElement('tr');
    row.innerHTML = `
        <td>
            <img src="${course.image}" class="checkout-item" >
        </td>
        <td>${course.title}</td>
        <td>${course.price}</td>
        <td>
            <a href="#" class="borrar-curso" data-id="${course.id}"> X </a>
        </td>
    `;

    listCourses.appendChild( row )

    saveToLocalStorage( course )
}

const saveToLocalStorage = ( course ) => {
    let courses;

    courses = getCourseLocalStorage()

    courses.push( course ) // selected course is added to LS

}

// CHECK IF ANY ELEMENT IN LS
const getCourseLocalStorage = () => {
    let coursesLS;

    if( localStorage.getItem('courses') === null) {
        coursesLS = []
    } else {
        // returns object courses
        coursesLS = JSON.parse( localStorage.getItem('courses') )
    }

    return coursesLS
}

// show courses from LS in cart
function readLS() {
    let coursesLS;

    coursesLS = getCourseLocalStorage() // pass data from getCourses function to make it available
    coursesLS.forEach( function( course ){
        const row = document.createElement('tr');
        row.innerHTML = `
            <td>
                <img src="${course.image}" class="checkout-item" >
            </td>
            <td>${course.title}</td>
            <td>${course.price}</td>
            <td>
                <a href="#" class="borrar-curso" data-id="${course.id}"> X </a>
            </td>
    `;

    listCourses.appendChild( row )
    })

}


function removeCourseLocalStorage ( course ) {
    let coursesLS;

    coursesLS = getCourseLocalStorage()

    coursesLS.forEach( function( courseLS, index){
        if( courseLS.id === course ) {
            coursesLS.splice(index, 1)
        }
    })

    localStorage.setItem('courses', JSON.stringify( coursesLS ))
}















